package com.kkp.intijaya.IntiJayaKKP.controllers;


import com.kkp.intijaya.IntiJayaKKP.dto.InvoiceDto;
import com.kkp.intijaya.IntiJayaKKP.model.DetailInvoice;
import com.kkp.intijaya.IntiJayaKKP.model.DetailPemesanan;
import com.kkp.intijaya.IntiJayaKKP.model.Invoice;
import com.kkp.intijaya.IntiJayaKKP.model.Pemesanan;
import com.kkp.intijaya.IntiJayaKKP.repositories.DetailInvoiceRepository;
import com.kkp.intijaya.IntiJayaKKP.repositories.InvoiceRepository;
import com.kkp.intijaya.IntiJayaKKP.repositories.PemesananRepository;
import com.kkp.intijaya.IntiJayaKKP.utils.GenerateVarID;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/v1/invoice")
public class InvoiceController {
    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    DetailInvoiceRepository detailInvoiceRepository;

    @Autowired
    PemesananRepository pemesananRepository;

    private ModelMapper modelMapper = new ModelMapper();
    private GenerateVarID generateVarID = new GenerateVarID();

    @PostMapping
    public ResponseEntity<JSONObject> createInvoice (@Validated @RequestBody InvoiceDto invoiceDto) throws IOException {
        JSONObject jsonObject = new JSONObject();

        Invoice invoice = modelMapper.map(invoiceRepository, Invoice.class);
        List<Invoice> invoiceList =  invoiceRepository.findAllByOrderByNoInvoiceAsc();
        String newId = invoiceList.size() > 0 ? generateVarID.getNewID(invoiceList.get(invoiceList.size()-1).getNoInvoice(), "INV-000") : "INV-0001";
        invoice.setNoInvoice(newId);
        invoiceRepository.save(invoice);

        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Invoice");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<JSONObject> getInvoice (){
        JSONObject jsonObject = new JSONObject();
        List<Invoice> invoiceList = invoiceRepository.findAll();
        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Invoice");
            jsonObject.put("data", invoiceList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping("/detailInvoiceList")
    public ResponseEntity<JSONObject> getDetailInvoice (){
        JSONObject jsonObject = new JSONObject();
        List<Invoice> invoiceList = invoiceRepository.findAll();
        List listInvoice = new ArrayList();

        for (Invoice invoice: invoiceList){
            Map map = new HashMap();
            map.put("invoice",invoice);
            List<DetailInvoice> detailInvoiceList = detailInvoiceRepository.findAllByInvoice(invoice);
            map.put("detailInvoiceList",detailInvoiceList);
            listInvoice.add(map);
        }
        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Invoice");
            jsonObject.put("data", listInvoice);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> updateInvoice (@Validated @RequestBody InvoiceDto invoiceDto){
        JSONObject jsonObject = new JSONObject();
        try{
            Invoice invoice = invoiceRepository.findByNoInvoice(invoiceDto.getNoInvoice());
            invoice.setTglInvoice(invoiceDto.getTglInvoice() !=null ? invoiceDto.getTglInvoice() : invoice.getTglInvoice());
            invoice.setJmlTagihan(invoiceDto.getJmlTagihan() !=null ? invoiceDto.getJmlTagihan() : invoice.getJmlTagihan());
            Pemesanan pemesanan = pemesananRepository.findByNoPemesanan(invoiceDto.getNoPemesanan());
            invoice.setPemesanan(invoiceDto.getNoPemesanan() !=null ? pemesanan : pemesanan);

            invoiceRepository.save(invoice);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success update Invoice");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<JSONObject> deleteInvoice (@Validated @RequestBody InvoiceDto invoiceDto){
        JSONObject jsonObject = new JSONObject();
        invoiceRepository.deleteByNoInvoice(invoiceDto.getNoInvoice());
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success delete Invoice");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }
}
