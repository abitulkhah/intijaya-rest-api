package com.kkp.intijaya.IntiJayaKKP.controllers;


import com.kkp.intijaya.IntiJayaKKP.dto.KwitansiDto;
import com.kkp.intijaya.IntiJayaKKP.model.Invoice;
import com.kkp.intijaya.IntiJayaKKP.model.Kwitansi;
import com.kkp.intijaya.IntiJayaKKP.model.Pemesanan;
import com.kkp.intijaya.IntiJayaKKP.repositories.InvoiceRepository;
import com.kkp.intijaya.IntiJayaKKP.repositories.KwitansiRepository;
import com.kkp.intijaya.IntiJayaKKP.utils.GenerateVarID;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/v1/kwitansi")
public class KwitansiController {
    @Autowired
    KwitansiRepository kwitansiRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    private ModelMapper modelMapper = new ModelMapper();
    private GenerateVarID generateVarID = new GenerateVarID();


    @PostMapping
    public ResponseEntity<JSONObject> createKwitansi (@Validated @RequestBody KwitansiDto kwitansiDto) throws IOException {
        JSONObject jsonObject = new JSONObject();

        Kwitansi kwitansi = modelMapper.map(kwitansiDto, Kwitansi.class);
        List<Kwitansi> kwitansiList =  kwitansiRepository.findAllByOrderByNoKwitansiAsc();
        String newId = kwitansiList.size() > 0 ? generateVarID.getNewID(kwitansiList.get(kwitansiList.size()-1).getNoKwitansi(), "KWI-000") : "KWI-0001";
        kwitansi.setNoKwitansi(newId);
        kwitansiRepository.save(kwitansi);

        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Kwitansi");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<JSONObject> getKwitansi (){
        JSONObject jsonObject = new JSONObject();
        List<Kwitansi> kwitansiList = kwitansiRepository.findAll();
        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Kwitansi");
            jsonObject.put("data", kwitansiList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping("/reportPembayaran")
    public ResponseEntity<JSONObject> getReportPembayaran (@RequestParam(name = "startDate", required = false) String startDate, @RequestParam(name = "endDate", required = false) String endDate) throws ParseException {
        System.out.println(startDate);
        System.out.println(startDate);
        JSONObject jsonObject = new JSONObject();
        List<Kwitansi> kwitansiList = new ArrayList<>();
        if(startDate!=null && endDate!=null){
            Date startDateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(startDate);
            Date endDateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate);
            kwitansiList = kwitansiRepository.findAllByTglKwitansiBetweenOrderByTglKwitansiAsc(startDateFormat, endDateFormat);
        }else{
            kwitansiList = kwitansiRepository.findAll();
        }

        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Kwitansi");
            jsonObject.put("data", kwitansiList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> updateKwitansi (@Validated @RequestBody KwitansiDto kwitansiDto){
        JSONObject jsonObject = new JSONObject();
        try{
            Kwitansi kwitansi = kwitansiRepository.findByNoKwitansi(kwitansiDto.getNoKwitansi());
            Invoice invoice = invoiceRepository.findByNoInvoice(kwitansiDto.getNoInvoice());
            kwitansi.setInvoice(invoice);
            kwitansi.setTglKwitansi(kwitansiDto.getTglKwitansi() !=null ? kwitansiDto.getTglKwitansi() : kwitansi.getTglKwitansi());
            kwitansi.setJmlBayar(kwitansiDto.getJmlBayar() !=null ? kwitansiDto.getJmlBayar() : kwitansi.getJmlBayar());

            kwitansiRepository.save(kwitansi);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success update Kwitansi");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<JSONObject> deleteKwitansi (@Validated @RequestBody KwitansiDto kwitansiDto){
        JSONObject jsonObject = new JSONObject();
        kwitansiRepository.deleteByNoKwitansi(kwitansiDto.getNoKwitansi());
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success delete Kwitansi");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }
}
