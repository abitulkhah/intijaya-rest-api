package com.kkp.intijaya.IntiJayaKKP.controllers;

import com.kkp.intijaya.IntiJayaKKP.dto.UserDto;
import com.kkp.intijaya.IntiJayaKKP.model.UserRole;
import com.kkp.intijaya.IntiJayaKKP.model.Users;
import com.kkp.intijaya.IntiJayaKKP.repositories.UserRoleRepository;
import com.kkp.intijaya.IntiJayaKKP.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private ModelMapper modelMapper = new ModelMapper();

    @GetMapping
    public List<Users> listUser(){
        return userService.findAll();
    }

    @PostMapping
    public Users create(@RequestBody UserDto userDto){
        Users users = modelMapper.map(userDto, Users.class);
        users.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userService.save(users);
        UserRole userRole = new UserRole();
        userRole.setRoleId(userDto.getRoleId());
        userRole.setUserId(users.getId());
        userRoleRepository.save(userRole);
        return users;
    }

    @DeleteMapping
    public String delete(@PathVariable(value = "id") Long id){
        userService.delete(id);
        return "success";
    }

}
