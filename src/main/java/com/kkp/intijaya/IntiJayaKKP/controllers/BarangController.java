package com.kkp.intijaya.IntiJayaKKP.controllers;

import com.kkp.intijaya.IntiJayaKKP.dto.BarangDto;
import com.kkp.intijaya.IntiJayaKKP.model.Barang;
import com.kkp.intijaya.IntiJayaKKP.model.ModelBarang;
import com.kkp.intijaya.IntiJayaKKP.repositories.BarangRepository;
import com.kkp.intijaya.IntiJayaKKP.repositories.ModelBarangRepository;
import com.kkp.intijaya.IntiJayaKKP.utils.GenerateVarID;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/v1/barang")
public class BarangController {
    @Autowired
    BarangRepository barangRepository;

    @Autowired
    ModelBarangRepository modelBarangRepository;

    private ModelMapper modelMapper = new ModelMapper();
    private GenerateVarID generateVarID = new GenerateVarID();

    @PostMapping
    public ResponseEntity<JSONObject> createBarang (@Validated @RequestBody BarangDto barangDto) throws IOException {
        JSONObject jsonObject = new JSONObject();

        Barang barang = modelMapper.map(barangDto, Barang.class);

        List<Barang> barangList =  barangRepository.findAllByOrderByKodeBarangAsc();
        String newId = barangList.size() > 0 ? generateVarID.getNewID(barangList.get(barangList.size()-1).getKodeBarang(), "BRG-000") : "BRG-0001";
        barang.setKodeBarang(newId);
        barangRepository.save(barang);

        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Barang");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<JSONObject> getBarang (){
        JSONObject jsonObject = new JSONObject();
        List<Barang> barangList = barangRepository.findAll();
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Barang");
            jsonObject.put("data", barangList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> updateBarang (@Validated @RequestBody BarangDto barangDto){
        JSONObject jsonObject = new JSONObject();
        try{
            Barang barang = barangRepository.findByKodeBarang(barangDto.getKodeBarang());
            barang.setNamaBarang(barangDto.getNamaBarang() !=null ? barangDto.getNamaBarang() : barang.getNamaBarang());
            barang.setHarga(barangDto.getHarga() !=null ? barangDto.getHarga() : barang.getHarga());
            ModelBarang modelBarang = modelBarangRepository.findByKodeModelBarang(barangDto.getKodeModelBarang() !=null ? barangDto.getKodeModelBarang() : barang.getModelBarang().getKodeModelBarang());
            barang.setModelBarang(modelBarang);
            barang.setSatuan(barangDto.getSatuan() !=null ? barangDto.getSatuan() : barang.getSatuan());
            barang.setStok(barangDto.getStok() !=null ? barangDto.getStok() : barang.getStok());

            barangRepository.save(barang);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success update Barang");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<JSONObject> deleteBarang (@Validated @RequestBody BarangDto barangDto){
        JSONObject jsonObject = new JSONObject();
        barangRepository.deleteByKodeBarang(barangDto.getKodeBarang());
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success delete Barang");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }
}

