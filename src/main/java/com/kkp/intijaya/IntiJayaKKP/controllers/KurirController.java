package com.kkp.intijaya.IntiJayaKKP.controllers;


import com.kkp.intijaya.IntiJayaKKP.dto.KurirDto;
import com.kkp.intijaya.IntiJayaKKP.model.Kurir;
import com.kkp.intijaya.IntiJayaKKP.repositories.KurirRepository;
import com.kkp.intijaya.IntiJayaKKP.utils.GenerateVarID;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/v1/kurir")
public class KurirController {
    @Autowired
    KurirRepository kurirRepository;

    private ModelMapper modelMapper = new ModelMapper();
    private GenerateVarID generateVarID = new GenerateVarID();


    @PostMapping
    public ResponseEntity<JSONObject> createKurir (@Validated @RequestBody KurirDto kurirDto) throws IOException {
        JSONObject jsonObject = new JSONObject();

        Kurir kurir = modelMapper.map(kurirDto, Kurir.class);
        List<Kurir> invoiceList =  kurirRepository.findAllByOrderByKodeKurirAsc();
        String newId = invoiceList.size() > 0 ? generateVarID.getNewID(invoiceList.get(invoiceList.size()-1).getKodeKurir(), "KRR-000") : "KRR-0001";
        kurir.setKodeKurir(newId);
        kurirRepository.save(kurir);

        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Kurir");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<JSONObject> getKurir (){
        JSONObject jsonObject = new JSONObject();
        List<Kurir> kurirList = kurirRepository.findAll();
        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Kurir");
            jsonObject.put("data", kurirList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> updateKurir (@Validated @RequestBody KurirDto kurirDto){
        JSONObject jsonObject = new JSONObject();
        try{
            Kurir kurir = kurirRepository.findByKodeKurir(kurirDto.getKodeKurir());
            kurir.setAlamatKurir(kurirDto.getAlamatKurir() !=null ? kurirDto.getAlamatKurir() : kurir.getAlamatKurir());
            kurir.setNamaKurir(kurirDto.getNamaKurir() !=null ? kurirDto.getNamaKurir() : kurir.getNamaKurir());
            kurir.setNoHp(kurirDto.getNoHp() !=null ? kurirDto.getNoHp() : kurir.getNoHp());

            kurirRepository.save(kurir);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success update Kurir");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<JSONObject> deleteKurir (@Validated @RequestBody KurirDto kurirDto){
        JSONObject jsonObject = new JSONObject();
        kurirRepository.deleteByKodeKurir(kurirDto.getKodeKurir());
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success delete Kurir");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }
}
