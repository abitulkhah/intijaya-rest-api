package com.kkp.intijaya.IntiJayaKKP.controllers;


import com.kkp.intijaya.IntiJayaKKP.dto.DetailInvoiceDto;
import com.kkp.intijaya.IntiJayaKKP.model.Barang;
import com.kkp.intijaya.IntiJayaKKP.model.DetailInvoice;
import com.kkp.intijaya.IntiJayaKKP.model.Invoice;
import com.kkp.intijaya.IntiJayaKKP.repositories.BarangRepository;
import com.kkp.intijaya.IntiJayaKKP.repositories.DetailInvoiceRepository;
import com.kkp.intijaya.IntiJayaKKP.repositories.InvoiceRepository;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/v1/detailInvoice")
public class DetailInvoiceController {
    @Autowired
    DetailInvoiceRepository detailInvoiceRepository;

    @Autowired
    BarangRepository barangRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    private ModelMapper modelMapper = new ModelMapper();

    @PostMapping
    public ResponseEntity<JSONObject> createDetailInvoice (@Validated @RequestBody DetailInvoiceDto detailInvoiceDto) throws IOException {
        JSONObject jsonObject = new JSONObject();

        DetailInvoice detailInvoice = modelMapper.map(detailInvoiceDto, DetailInvoice.class);
        detailInvoiceRepository.save(detailInvoice);

        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Detail Invoice");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<JSONObject> getDetailInvoice (){
        JSONObject jsonObject = new JSONObject();
        List<DetailInvoice> detailInvoiceList = detailInvoiceRepository.findAll();
        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Detail Invoice");
            jsonObject.put("data", detailInvoiceList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> updateDetailInvoice (@Validated @RequestBody DetailInvoiceDto detailInvoiceDto){
        JSONObject jsonObject = new JSONObject();
        try{
            DetailInvoice detailInvoice = detailInvoiceRepository.findById(detailInvoiceDto.getId()).get();
            Barang barang = barangRepository.findByKodeBarang(detailInvoiceDto.getKodeBarang() !=null ? detailInvoiceDto.getKodeBarang() : detailInvoice.getKodeBarang());
            detailInvoice.setKodeBarang(detailInvoiceDto.getKodeBarang());
            detailInvoice.setHargaBarang(barang.getHarga());
            Invoice invoice = invoiceRepository.findByNoInvoice(detailInvoiceDto.getNoInvoice() !=null ? detailInvoiceDto.getNoInvoice() : detailInvoice.getInvoice().getNoInvoice());
            detailInvoice.setInvoice(invoice);
            detailInvoice.setQuantity(detailInvoiceDto.getQuantity() !=null ? detailInvoiceDto.getQuantity() : detailInvoice.getQuantity());

            detailInvoiceRepository.save(detailInvoice);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success update Detail Invoice");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<JSONObject> deleteDetailInvoice (@Validated @RequestBody DetailInvoiceDto detailInvoiceDto){
        JSONObject jsonObject = new JSONObject();
        detailInvoiceRepository.deleteById(detailInvoiceDto.getId());
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success delete Detail Invoice");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }
}
