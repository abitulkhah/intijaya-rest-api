package com.kkp.intijaya.IntiJayaKKP.controllers;

import com.kkp.intijaya.IntiJayaKKP.dto.SuratJalanDto;
import com.kkp.intijaya.IntiJayaKKP.model.*;
import com.kkp.intijaya.IntiJayaKKP.repositories.*;
import com.kkp.intijaya.IntiJayaKKP.utils.GenerateVarID;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/v1/suratJalan")
public class SuratJalanController {
    @Autowired
    SuratJalanRepository suratJalanRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    KendaraanRepository kendaraanRepository;

    @Autowired
    ReceiptRepository receiptRepository;

    @Autowired
    KurirRepository kurirRepository;

    private ModelMapper modelMapper = new ModelMapper();
    private GenerateVarID generateVarID = new GenerateVarID();

    @PostMapping
    public ResponseEntity<JSONObject> createSuratJalan (@Validated @RequestBody SuratJalanDto suratJalanDto) throws IOException {
        JSONObject jsonObject = new JSONObject();

        SuratJalan suratJalan = modelMapper.map(suratJalanDto, SuratJalan.class);
        List<SuratJalan> suratJalanList =  suratJalanRepository.findAllByOrderByNoSuratJalanAsc();
        String newId = suratJalanList.size() > 0 ? generateVarID.getNewID(suratJalanList.get(suratJalanList.size()-1).getNoSuratJalan(), "SJ-000") : "SJ-0001";
        suratJalan.setNoSuratJalan(newId);
        suratJalanRepository.save(suratJalan);


        Receipt receipt = new Receipt();
        List<Receipt> receiptList =  receiptRepository.findAllByOrderByNoReceiptAsc();
        String newReceiptId = receiptList.size() > 0 ? generateVarID.getNewID(receiptList.get(receiptList.size()-1).getNoReceipt(), "RC-000") : "RC-0001";
        receipt.setNoReceipt(newReceiptId);
        receipt.setSuratJalan(suratJalan);
        receipt.setTglReceipt(new Date());
        receiptRepository.save(receipt);

        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Surat Jalan");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<JSONObject> getSuratJalan (){
        JSONObject jsonObject = new JSONObject();
        List<SuratJalan> suratJalanList = suratJalanRepository.findAll();
        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Surat Jalan");
            jsonObject.put("data", suratJalanList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @GetMapping("/reportPengirimanBarang")
    public ResponseEntity<JSONObject> getReportPengirimanBarang (@RequestParam(name = "startDate", required = false) String startDate, @RequestParam(name = "endDate", required = false) String endDate) throws ParseException {
        System.out.println(startDate);
        System.out.println(startDate);
        JSONObject jsonObject = new JSONObject();
        List<SuratJalan> suratJalanList = new ArrayList<>();
        if(startDate!=null && endDate!=null){
            Date startDateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(startDate);
            Date endDateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate);
            suratJalanList = suratJalanRepository.findAllByTglSuratJalanBetweenOrderByTglSuratJalanAsc(startDateFormat, endDateFormat);
        }else{
            suratJalanList = suratJalanRepository.findAll();
        }

        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Pengiriman Barang");
            jsonObject.put("data", suratJalanList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> updateSuratJalan (@Validated @RequestBody SuratJalanDto suratJalanDto){
        JSONObject jsonObject = new JSONObject();
        try{

            SuratJalan suratJalan = suratJalanRepository.findByNoSuratJalan(suratJalanDto.getNoSuratJalan());
            suratJalan.setAlamatPenerima(suratJalanDto.getAlamatPenerima() !=null ? suratJalanDto.getAlamatPenerima() : suratJalan.getAlamatPenerima());
            suratJalan.setNamaPenerima(suratJalanDto.getNamaPenerima() !=null ? suratJalanDto.getNamaPenerima() : suratJalan.getNamaPenerima());
            suratJalan.setTglSuratJalan(suratJalanDto.getTglSuratJalan() !=null ? suratJalanDto.getTglSuratJalan() : suratJalan.getTglSuratJalan());

            Kurir kurir = kurirRepository.findByKodeKurir(suratJalanDto.getKodeKurir() !=null ? suratJalanDto.getKodeKurir() : suratJalan.getKurir().getKodeKurir());
            suratJalan.setKurir(kurir);

            Kendaraan kendaraan = kendaraanRepository.findByKodeKendaraan(suratJalanDto.getKodeKendaraan() !=null ? suratJalanDto.getKodeKendaraan() : suratJalan.getKendaraan().getKodeKendaraan());
            suratJalan.setKendaraan(kendaraan);

            Invoice invoice = invoiceRepository.findByNoInvoice(suratJalanDto.getNoInvoice() !=null ? suratJalanDto.getNoInvoice() : suratJalan.getInvoice().getNoInvoice());
            suratJalan.setInvoice(invoice);

            suratJalanRepository.save(suratJalan);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success update Surat Jalan");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<JSONObject> deleteSuratJalan (@Validated @RequestBody SuratJalanDto suratJalanDto){
        JSONObject jsonObject = new JSONObject();
        suratJalanRepository.deleteByNoSuratJalan(suratJalanDto.getNoSuratJalan());
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success delete Surat Jalan");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }
}
