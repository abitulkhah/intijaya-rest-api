package com.kkp.intijaya.IntiJayaKKP.controllers;

import com.kkp.intijaya.IntiJayaKKP.dto.ModelBarangDto;
import com.kkp.intijaya.IntiJayaKKP.model.ModelBarang;
import com.kkp.intijaya.IntiJayaKKP.repositories.ModelBarangRepository;
import com.kkp.intijaya.IntiJayaKKP.utils.GenerateVarID;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/v1/modelBarang")
public class ModelBarangController {
    @Autowired
    ModelBarangRepository modelBarangRepository;

    private ModelMapper modelMapper = new ModelMapper();
    private GenerateVarID generateVarID = new GenerateVarID();

    @PostMapping
    public ResponseEntity<JSONObject> createModelBarang (@Validated @RequestBody ModelBarangDto modelBarangDto) throws IOException {
        JSONObject jsonObject = new JSONObject();

        ModelBarang modelBarang = modelMapper.map(modelBarangDto, ModelBarang.class);
        List<ModelBarang> modelBarangList =  modelBarangRepository.findAllByOrderByKodeModelBarangAsc();
        String newId = modelBarangList.size() > 0 ? generateVarID.getNewID(modelBarangList.get(modelBarangList.size()-1).getKodeModelBarang(), "MDL-000") : "MDL-0001";
        modelBarang.setKodeModelBarang(newId);
        modelBarangRepository.save(modelBarang);

        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Model Barang");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<JSONObject> getModelBarang (){
        JSONObject jsonObject = new JSONObject();
        List<ModelBarang> modelBarangList = modelBarangRepository.findAll();
        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Model Barang");
            jsonObject.put("data", modelBarangList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> updateModelBarang (@Validated @RequestBody ModelBarangDto modelBarangDto){
        JSONObject jsonObject = new JSONObject();
        try{
            ModelBarang modelBarang = modelBarangRepository.findByKodeModelBarang(modelBarangDto.getKodeModelBarang());
            modelBarang.setType(modelBarangDto.getType() !=null ? modelBarangDto.getType() : modelBarang.getType());

            modelBarangRepository.save(modelBarang);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success update Model Barang");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<JSONObject> deleteModelBarang (@Validated @RequestBody ModelBarangDto modelBarangDto){
        JSONObject jsonObject = new JSONObject();
        modelBarangRepository.deleteByKodeModelBarang(modelBarangDto.getKodeModelBarang());
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success delete Model Barang");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }
}
