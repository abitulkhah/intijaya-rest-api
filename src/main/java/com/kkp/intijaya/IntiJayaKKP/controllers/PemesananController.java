package com.kkp.intijaya.IntiJayaKKP.controllers;


import com.kkp.intijaya.IntiJayaKKP.dto.DetailPemesananDto;
import com.kkp.intijaya.IntiJayaKKP.dto.PemesananDto;
import com.kkp.intijaya.IntiJayaKKP.model.*;
import com.kkp.intijaya.IntiJayaKKP.repositories.*;
import com.kkp.intijaya.IntiJayaKKP.utils.GenerateVarID;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/v1/pemesanan")
public class PemesananController {
    @Autowired
    PemesananRepository pemesananRepository;

    @Autowired
    DetailPemesananRepository detailPemesananRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    DetailInvoiceRepository detailInvoiceRepository;

    @Autowired
    KwitansiRepository kwitansiRepository;


    @Autowired
    BarangRepository barangRepository;

    private ModelMapper modelMapper = new ModelMapper();
    private GenerateVarID generateVarID = new GenerateVarID();

    @PostMapping
    public ResponseEntity<JSONObject> createPemesanan (@Validated @RequestBody PemesananDto pemesananDto) throws IOException {
        JSONObject jsonObject = new JSONObject();

        Pemesanan pemesanan = modelMapper.map(pemesananDto, Pemesanan.class);
        List<Pemesanan> pemesananList =  pemesananRepository.findAllByOrderByNoPemesananAsc();
        String newId = pemesananList.size() > 0 ? generateVarID.getNewID(pemesananList.get(pemesananList.size()-1).getNoPemesanan(), "PMSA-000") : "PMSA-0001";
        pemesanan.setNoPemesanan(newId);
        pemesananRepository.save(pemesanan);


//        for detail pemesanan
        for (DetailPemesananDto detailPemesananDto: pemesananDto.getDetailPemesananDtoList()){
            DetailPemesanan detailPemesanan =  new DetailPemesanan();
            detailPemesanan.setPemesanan(pemesanan);
            Barang barang = barangRepository.findByKodeBarang(detailPemesananDto.getKodeBarang());
            detailPemesanan.setBarang(barang);
            detailPemesanan.setKetBarang(detailPemesananDto.getKetBarang());
            detailPemesanan.setJmlPemesanan(detailPemesananDto.getJmlPemesanan());
            barang.setStok(barang.getStok()-detailPemesananDto.getJmlPemesanan());
            detailPemesananRepository.save(detailPemesanan);
        }

//        for invoice

        Invoice invoice = new Invoice();
        List<Invoice> invoiceList =  invoiceRepository.findAllByOrderByNoInvoiceAsc();
        String newInvoiceId = invoiceList.size() > 0 ? generateVarID.getNewID(invoiceList.get(invoiceList.size()-1).getNoInvoice(), "INV-000") : "INV-0001";
        invoice.setNoInvoice(newInvoiceId);
        invoice.setPemesanan(pemesanan);
        invoice.setTglInvoice(new Date());
        invoice.setJmlTagihan(pemesananDto.getGrandTotal());
        invoiceRepository.save(invoice);

//        for detail invoice

        for (DetailPemesananDto detailPemesananDto: pemesananDto.getDetailPemesananDtoList()){
            DetailInvoice detailInvoice =  new DetailInvoice();
            Barang barang = barangRepository.findByKodeBarang(detailPemesananDto.getKodeBarang());
            detailInvoice.setKodeBarang(detailPemesananDto.getKodeBarang());
            detailInvoice.setQuantity(detailPemesananDto.getJmlPemesanan());
            detailInvoice.setInvoice(invoice);
            detailInvoice.setHargaBarang(barang.getHarga());
            detailInvoice.setNamaBarang(barang.getNamaBarang());
            detailInvoiceRepository.save(detailInvoice);
        }

//        for kwitansi
        Kwitansi kwitansi = new Kwitansi();
        List<Kwitansi> kwitansiList =  kwitansiRepository.findAllByOrderByNoKwitansiAsc();
        String newKwitansiId = kwitansiList.size() > 0 ? generateVarID.getNewID(kwitansiList.get(kwitansiList.size()-1).getNoKwitansi(), "KWI-000") : "KWI-0001";
        kwitansi.setNoKwitansi(newKwitansiId);
        kwitansi.setJmlBayar(pemesananDto.getDp());
        kwitansi.setTglKwitansi(new Date());
        kwitansi.setInvoice(invoice);
        kwitansiRepository.save(kwitansi);

        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Pemesanan");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<JSONObject> getPemesanan (){
        JSONObject jsonObject = new JSONObject();
        List<Pemesanan> pemesananList = pemesananRepository.findAll();
        List listPemesanan = new ArrayList();

        for (Pemesanan pemesanan: pemesananList){
            Map map = new HashMap();
            map.put("pemesanan",pemesanan);
            List<DetailPemesanan> detailPemesananList = detailPemesananRepository.findAllByPemesanan(pemesanan);
            map.put("detailPemesananList", detailPemesananList);
            listPemesanan.add(map);
        }
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Pemesanan");
            jsonObject.put("data", listPemesanan);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping("/reportPenjualan")
    public ResponseEntity<JSONObject> getReportPenjualan (@RequestParam(name = "startDate", required = false) String startDate, @RequestParam(name = "endDate", required = false) String endDate) throws ParseException {
        System.out.println(startDate);
        System.out.println(startDate);
        JSONObject jsonObject = new JSONObject();
        List<Pemesanan> pemesananList = new ArrayList<>();
        if(startDate!=null && endDate!=null){
            Date startDateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(startDate);
            Date endDateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate);
            pemesananList = pemesananRepository.findAllByTglPemesananBetween(startDateFormat, endDateFormat);
        }else{
            pemesananList = pemesananRepository.findAll();
        }

        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Pemesanan");
            jsonObject.put("data", pemesananList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @PutMapping
    public ResponseEntity<JSONObject> updatePemesanan (@Validated @RequestBody PemesananDto pemesananDto){
        JSONObject jsonObject = new JSONObject();
        try{
            Pemesanan pemesanan = pemesananRepository.findByNoPemesanan(pemesananDto.getNoPemesanan());
            pemesanan.setDp(pemesananDto.getDp() !=null ? pemesananDto.getDp() : pemesanan.getDp());
            pemesanan.setGrandTotal(pemesananDto.getGrandTotal() !=null ? pemesananDto.getGrandTotal() : pemesanan.getGrandTotal());
            pemesanan.setSisaBayar(pemesananDto.getSisaBayar() !=null ? pemesananDto.getSisaBayar() : pemesanan.getSisaBayar());
            pemesanan.setTglPemesanan(pemesananDto.getTglPemesanan() !=null ? pemesananDto.getTglPemesanan() : pemesanan.getTglPemesanan());

            pemesananRepository.save(pemesanan);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success update Pemesanan");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<JSONObject> deletePemesanan (@Validated @RequestBody PemesananDto pemesananDto){
        JSONObject jsonObject = new JSONObject();
        pemesananRepository.deleteByNoPemesanan(pemesananDto.getNoPemesanan());
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success delete Pemesanan");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }
}
