package com.kkp.intijaya.IntiJayaKKP.controllers;


import com.kkp.intijaya.IntiJayaKKP.dto.DetailReceiptDto;
import com.kkp.intijaya.IntiJayaKKP.model.Barang;
import com.kkp.intijaya.IntiJayaKKP.model.DetailReceipt;
import com.kkp.intijaya.IntiJayaKKP.model.Pemesanan;
import com.kkp.intijaya.IntiJayaKKP.model.Receipt;
import com.kkp.intijaya.IntiJayaKKP.repositories.BarangRepository;
import com.kkp.intijaya.IntiJayaKKP.repositories.DetailReceiptRepository;
import com.kkp.intijaya.IntiJayaKKP.repositories.ReceiptRepository;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/v1/detailReceipt")
public class DetailReceiptController {
    @Autowired
    DetailReceiptRepository detailReceiptRepository;

    @Autowired
    ReceiptRepository receiptRepository;

    @Autowired
    BarangRepository barangRepository;

    private ModelMapper modelMapper = new ModelMapper();

    @PostMapping
    public ResponseEntity<JSONObject> createDetailReceipt (@Validated @RequestBody DetailReceiptDto detailReceiptDto) throws IOException {
        JSONObject jsonObject = new JSONObject();

        DetailReceipt detailReceipt = modelMapper.map(detailReceiptDto, DetailReceipt.class);
        detailReceiptRepository.save(detailReceipt);

        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Detail Receipt");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<JSONObject> getDetailReceipt (){
        JSONObject jsonObject = new JSONObject();
        List<DetailReceipt> detailReceiptList = detailReceiptRepository.findAll();
        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Detail Receipt");
            jsonObject.put("data", detailReceiptList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping("/reportReturBarang")
    public ResponseEntity<JSONObject> getReportReturBarang (@RequestParam(name = "startDate", required = false) String startDate, @RequestParam(name = "endDate", required = false) String endDate) throws ParseException {
        System.out.println(startDate);
        System.out.println(startDate);
        JSONObject jsonObject = new JSONObject();
        List<DetailReceipt> detailReceiptList = new ArrayList<>();
        if(startDate!=null && endDate!=null){
            Date startDateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(startDate);
            Date endDateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate);
            detailReceiptList = detailReceiptRepository.findAllByTglReturBetweenOrderByReceiptAsc(startDateFormat, endDateFormat);
        }else{
            detailReceiptList = detailReceiptRepository.findAll();
        }

        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Retur Barang");
            jsonObject.put("data", detailReceiptList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> updateDetailReceipt (@Validated @RequestBody DetailReceiptDto detailReceiptDto){
        JSONObject jsonObject = new JSONObject();
        try{
            DetailReceipt detailReceipt = detailReceiptRepository.findById(detailReceiptDto.getId()).get();
            detailReceipt.setJmlRetur(detailReceiptDto.getJmlRetur() !=null ? detailReceiptDto.getJmlRetur() : detailReceipt.getJmlRetur());
            detailReceipt.setKetRetur(detailReceiptDto.getKetRetur() !=null ? detailReceiptDto.getKetRetur() : detailReceipt.getKetRetur());

            Barang barang = barangRepository.findByKodeBarang(detailReceiptDto.getKodeBarang());
            Receipt receipt = receiptRepository.findByNoReceipt(detailReceiptDto.getNoReceipt() !=null ? detailReceiptDto.getNoReceipt() : detailReceipt.getReceipt().getNoReceipt());
            detailReceipt.setBarang(barang);
            detailReceipt.setReceipt(receipt);
            detailReceipt.setTglRetur(detailReceiptDto.getTglRetur() !=null ? detailReceiptDto.getTglRetur() : detailReceipt.getTglRetur());

            detailReceiptRepository.save(detailReceipt);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success update Detail Receipt");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<JSONObject> deleteDetailReceipt (@Validated @RequestBody DetailReceiptDto detailReceiptDto){
        JSONObject jsonObject = new JSONObject();
        detailReceiptRepository.deleteById(detailReceiptDto.getId());
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success delete Detail Receipt");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }
}
