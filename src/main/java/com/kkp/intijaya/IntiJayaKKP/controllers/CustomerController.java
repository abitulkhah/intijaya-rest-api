package com.kkp.intijaya.IntiJayaKKP.controllers;

import com.kkp.intijaya.IntiJayaKKP.dto.CustomerDto;
import com.kkp.intijaya.IntiJayaKKP.model.Customer;
import com.kkp.intijaya.IntiJayaKKP.repositories.CustomerRepository;
import com.kkp.intijaya.IntiJayaKKP.utils.GenerateVarID;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/v1/customer")
public class CustomerController {
    @Autowired
    CustomerRepository customerRepository;

    private ModelMapper modelMapper = new ModelMapper();
    private GenerateVarID generateVarID = new GenerateVarID();

    @PostMapping
    public ResponseEntity<JSONObject> createCustomer (@Validated @RequestBody CustomerDto customerDto) throws IOException {
        JSONObject jsonObject = new JSONObject();

        Customer customer = modelMapper.map(customerDto, Customer.class);
        List<Customer> customerList =  customerRepository.findAllByOrderByNoCustomerAsc();
        String newId = customerList.size() > 0 ? generateVarID.getNewID(customerList.get(customerList.size()-1).getNoCustomer(), "CUS-000") : "CUS-0001";
        customer.setNoCustomer(newId);
        customerRepository.save(customer);

        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Customer");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<JSONObject> getCustomer (){
        JSONObject jsonObject = new JSONObject();
        List<Customer> customerList = customerRepository.findAll();
        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Customer");
            jsonObject.put("data", customerList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> updateCustomer (@Validated @RequestBody CustomerDto customerDto){
        JSONObject jsonObject = new JSONObject();
        try{
            Customer customer = customerRepository.findByNoCustomer(customerDto.getNoCustomer());
            customer.setAlamat(customerDto.getAlamat() !=null ? customerDto.getAlamat() : customer.getAlamat());
            customer.setNamaCustomer(customerDto.getNamaCustomer() !=null ? customerDto.getNamaCustomer() : customer.getNamaCustomer());
            customer.setNoHp(customerDto.getNoHp() !=null ? customerDto.getNoHp() : customer.getNoHp());

            customerRepository.save(customer);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success update Customer");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<JSONObject> deleteCustomer (@Validated @RequestBody CustomerDto customerDto){
        JSONObject jsonObject = new JSONObject();
        customerRepository.deleteByNoCustomer(customerDto.getNoCustomer());
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success delete Customer");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }
}
