package com.kkp.intijaya.IntiJayaKKP.controllers;


import com.kkp.intijaya.IntiJayaKKP.dto.KendaraanDto;
import com.kkp.intijaya.IntiJayaKKP.model.Kendaraan;
import com.kkp.intijaya.IntiJayaKKP.repositories.KendaraanRepository;
import com.kkp.intijaya.IntiJayaKKP.utils.GenerateVarID;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/v1/kendaraan")
public class KendaraanController {
    @Autowired
    KendaraanRepository kendaraanRepository;

    private ModelMapper modelMapper = new ModelMapper();
    private GenerateVarID generateVarID = new GenerateVarID();


    @PostMapping
    public ResponseEntity<JSONObject> createKendaraan (@Validated @RequestBody KendaraanDto kendaraanDto) throws IOException {
        JSONObject jsonObject = new JSONObject();

        Kendaraan kendaraan = modelMapper.map(kendaraanDto, Kendaraan.class);
        List<Kendaraan> kendaraanList =  kendaraanRepository.findAllByOrderByKodeKendaraanAsc();
        String newId = kendaraanList.size() > 0 ? generateVarID.getNewID(kendaraanList.get(kendaraanList.size()-1).getKodeKendaraan(), "KNDR-000") : "KNDR-0001";
        kendaraan.setKodeKendaraan(newId);
        kendaraanRepository.save(kendaraan);

        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Kendaraan");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<JSONObject> getKendaraan (){
        JSONObject jsonObject = new JSONObject();
        List<Kendaraan> kendaraanList = kendaraanRepository.findAll();
        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Kendaraan");
            jsonObject.put("data", kendaraanList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> updateKendaraan (@Validated @RequestBody KendaraanDto kendaraanDto){
        JSONObject jsonObject = new JSONObject();
        try{
            Kendaraan kendaraan = kendaraanRepository.findByKodeKendaraan(kendaraanDto.getKodeKendaraan());
            kendaraan.setNamaKendaraan(kendaraanDto.getNamaKendaraan() !=null ? kendaraanDto.getNamaKendaraan() : kendaraan.getNamaKendaraan());
            kendaraan.setTypeKendaraan(kendaraanDto.getTypeKendaraan() !=null ? kendaraanDto.getTypeKendaraan() : kendaraan.getTypeKendaraan());
            kendaraan.setNopol(kendaraanDto.getNopol() !=null ? kendaraanDto.getNopol() : kendaraan.getNopol());

            kendaraanRepository.save(kendaraan);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success update Kendaraan");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<JSONObject> deleteKendaraan (@Validated @RequestBody KendaraanDto kendaraanDto){
        JSONObject jsonObject = new JSONObject();
        kendaraanRepository.deleteByKodeKendaraan(kendaraanDto.getKodeKendaraan());
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success delete Kendaraan");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }
}
