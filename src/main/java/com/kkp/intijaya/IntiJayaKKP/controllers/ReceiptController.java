package com.kkp.intijaya.IntiJayaKKP.controllers;

import com.kkp.intijaya.IntiJayaKKP.dto.ReceiptDto;
import com.kkp.intijaya.IntiJayaKKP.model.Receipt;
import com.kkp.intijaya.IntiJayaKKP.model.SuratJalan;
import com.kkp.intijaya.IntiJayaKKP.repositories.ReceiptRepository;
import com.kkp.intijaya.IntiJayaKKP.repositories.SuratJalanRepository;
import com.kkp.intijaya.IntiJayaKKP.utils.GenerateVarID;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/v1/receipt")
public class ReceiptController {
    @Autowired
    ReceiptRepository receiptRepository;

    @Autowired
    SuratJalanRepository suratJalanRepository;

    private ModelMapper modelMapper = new ModelMapper();
    private GenerateVarID generateVarID = new GenerateVarID();

    @PostMapping
    public ResponseEntity<JSONObject> createReceipt (@Validated @RequestBody ReceiptDto receiptDto) throws IOException {
        JSONObject jsonObject = new JSONObject();

        Receipt receipt = modelMapper.map(receiptDto, Receipt.class);
        List<Receipt> receiptList =  receiptRepository.findAllByOrderByNoReceiptAsc();
        String newId = receiptList.size() > 0 ? generateVarID.getNewID(receiptList.get(receiptList.size()-1).getNoReceipt(), "REC-000") : "REC-0001";
        receipt.setNoReceipt(newId);
        receiptRepository.save(receipt);

        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Receipt");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<JSONObject> getReceipt (){
        JSONObject jsonObject = new JSONObject();
        List<Receipt> receiptList = receiptRepository.findAll();
        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Receipt");
            jsonObject.put("data", receiptList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> updateReceipt (@Validated @RequestBody ReceiptDto receiptDto){
        JSONObject jsonObject = new JSONObject();
        try{
            Receipt receipt = receiptRepository.findByNoReceipt(receiptDto.getNoReceipt());
            receipt.setTglReceipt(receiptDto.getTglReceipt() !=null ? receiptDto.getTglReceipt() : receipt.getTglReceipt());
            SuratJalan suratJalan = suratJalanRepository.findByNoSuratJalan(receiptDto.getNoSuratJalan() !=null ? receiptDto.getNoSuratJalan() : receipt.getSuratJalan().getNoSuratJalan());
            receipt.setSuratJalan(suratJalan);
            
            receiptRepository.save(receipt);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success update Receipt");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<JSONObject> deleteReceipt (@Validated @RequestBody ReceiptDto receiptDto){
        JSONObject jsonObject = new JSONObject();
        receiptRepository.deleteByNoReceipt(receiptDto.getNoReceipt());
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success delete Receipt");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }
}
