package com.kkp.intijaya.IntiJayaKKP.controllers;


import com.kkp.intijaya.IntiJayaKKP.dto.DetailPemesananDto;
import com.kkp.intijaya.IntiJayaKKP.model.Barang;
import com.kkp.intijaya.IntiJayaKKP.model.DetailPemesanan;
import com.kkp.intijaya.IntiJayaKKP.model.Pemesanan;
import com.kkp.intijaya.IntiJayaKKP.repositories.BarangRepository;
import com.kkp.intijaya.IntiJayaKKP.repositories.DetailPemesananRepository;
import com.kkp.intijaya.IntiJayaKKP.repositories.PemesananRepository;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/v1/detailPemesanan")
public class DetailPemesananController {
    @Autowired
    DetailPemesananRepository detailPemesananRepository;

    @Autowired
    BarangRepository barangRepository;

    @Autowired
    PemesananRepository pemesananRepository;

    private ModelMapper modelMapper = new ModelMapper();

    @PostMapping
    public ResponseEntity<JSONObject> createDetailPemesanan (@Validated @RequestBody DetailPemesananDto detailPemesananDto) throws IOException {
        JSONObject jsonObject = new JSONObject();

        DetailPemesanan detailPemesanan = modelMapper.map(detailPemesananDto, DetailPemesanan.class);
        detailPemesananRepository.save(detailPemesanan);

        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Detail Pemesanan");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<JSONObject> getDetailPemesanan (){
        JSONObject jsonObject = new JSONObject();
        List<DetailPemesanan> detailPemesananList = detailPemesananRepository.findAll();
        try{

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Detail Pemesanan");
            jsonObject.put("data", detailPemesananList);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> updateDetailPemesanan (@Validated @RequestBody DetailPemesananDto detailPemesananDto){
        JSONObject jsonObject = new JSONObject();
        try{
            DetailPemesanan detailPemesanan = detailPemesananRepository.findById(detailPemesananDto.getId()).get();
            detailPemesanan.setJmlPemesanan(detailPemesananDto.getJmlPemesanan() !=null ? detailPemesananDto.getJmlPemesanan() : detailPemesanan.getJmlPemesanan());
            detailPemesanan.setKetBarang(detailPemesananDto.getKetBarang() !=null ? detailPemesananDto.getKetBarang() : detailPemesanan.getKetBarang());
            Barang barang = barangRepository.findByKodeBarang(detailPemesananDto.getKodeBarang() !=null ? detailPemesananDto.getKodeBarang() : detailPemesanan.getBarang().getKodeBarang());
            detailPemesanan.setBarang(barang);

            Pemesanan pemesanan = pemesananRepository.findByNoPemesanan(detailPemesananDto.getNoPemesanan() !=null ? detailPemesananDto.getNoPemesanan() : detailPemesanan.getPemesanan().getNoPemesanan());
            detailPemesanan.setPemesanan(pemesanan);

            detailPemesananRepository.save(detailPemesanan);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success update Detail Pemesanan");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<JSONObject> deleteDetailPemesanan (@Validated @RequestBody DetailPemesananDto detailPemesananDto){
        JSONObject jsonObject = new JSONObject();
        detailPemesananRepository.deleteById(detailPemesananDto.getId());
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success delete Detail Pemesanan");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }
}
