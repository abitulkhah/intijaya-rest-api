package com.kkp.intijaya.IntiJayaKKP.dao;

import com.kkp.intijaya.IntiJayaKKP.model.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends CrudRepository<Users,Long> {
    Users findByUsername(String username);
}
