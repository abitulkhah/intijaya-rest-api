package com.kkp.intijaya.IntiJayaKKP.services;


import com.kkp.intijaya.IntiJayaKKP.model.Users;

import java.util.List;

public interface UserService {

    Users save(Users user);

    List<Users> findAll();
    void delete(long id);
}
