package com.kkp.intijaya.IntiJayaKKP.dto;

import java.util.Date;
import java.util.List;

public class PemesananDto {

    private String noPemesanan;
    private Date tglPemesanan;
    private Double dp;
    private Double sisaBayar;
    private Double grandTotal;
    private String noCustomer;
    private List<DetailPemesananDto> detailPemesananDtoList;

    public String getNoCustomer() {
        return noCustomer;
    }

    public void setNoCustomer(String noCustomer) {
        this.noCustomer = noCustomer;
    }

    public String getNoPemesanan() {
        return noPemesanan;
    }

    public void setNoPemesanan(String noPemesanan) {
        this.noPemesanan = noPemesanan;
    }

    public Date getTglPemesanan() {
        return tglPemesanan;
    }

    public void setTglPemesanan(Date tglPemesanan) {
        this.tglPemesanan = tglPemesanan;
    }

    public Double getDp() {
        return dp;
    }

    public void setDp(Double dp) {
        this.dp = dp;
    }

    public Double getSisaBayar() {
        return sisaBayar;
    }

    public void setSisaBayar(Double sisaBayar) {
        this.sisaBayar = sisaBayar;
    }

    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public List<DetailPemesananDto> getDetailPemesananDtoList() {
        return detailPemesananDtoList;
    }

    public void setDetailPemesananDtoList(List<DetailPemesananDto> detailPemesananDtoList) {
        this.detailPemesananDtoList = detailPemesananDtoList;
    }
}
