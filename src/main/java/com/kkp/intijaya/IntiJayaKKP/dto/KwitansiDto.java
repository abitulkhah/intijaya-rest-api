package com.kkp.intijaya.IntiJayaKKP.dto;

import java.util.Date;

public class KwitansiDto {
    private String noKwitansi;
    private Date tglKwitansi;
    private Double jmlBayar;
    private String noInvoice;

    public String getNoKwitansi() {
        return noKwitansi;
    }

    public void setNoKwitansi(String noKwitansi) {
        this.noKwitansi = noKwitansi;
    }

    public Date getTglKwitansi() {
        return tglKwitansi;
    }

    public void setTglKwitansi(Date tglKwitansi) {
        this.tglKwitansi = tglKwitansi;
    }

    public Double getJmlBayar() {
        return jmlBayar;
    }

    public void setJmlBayar(Double jmlBayar) {
        this.jmlBayar = jmlBayar;
    }

    public String getNoInvoice() {
        return noInvoice;
    }

    public void setNoInvoice(String noInvoice) {
        this.noInvoice = noInvoice;
    }
}
