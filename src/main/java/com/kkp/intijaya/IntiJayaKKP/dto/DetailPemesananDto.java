package com.kkp.intijaya.IntiJayaKKP.dto;

import java.util.Date;

public class DetailPemesananDto {

    private long id;
    private String noPemesanan;
    private String kodeBarang;
    private Integer jmlPemesanan;
    private String ketBarang;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNoPemesanan() {
        return noPemesanan;
    }

    public void setNoPemesanan(String noPemesanan) {
        this.noPemesanan = noPemesanan;
    }

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public String getKetBarang() {
        return ketBarang;
    }

    public void setKetBarang(String ketBarang) {
        this.ketBarang = ketBarang;
    }

    public Integer getJmlPemesanan() {
        return jmlPemesanan;
    }

    public void setJmlPemesanan(Integer jmlPemesanan) {
        this.jmlPemesanan = jmlPemesanan;
    }

}
