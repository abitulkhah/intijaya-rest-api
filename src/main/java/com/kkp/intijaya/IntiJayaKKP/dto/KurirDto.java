package com.kkp.intijaya.IntiJayaKKP.dto;

public class KurirDto {

    private String kodeKurir;
    private String namaKurir;
    private String alamatKurir;
    private String noHp;


    public String getKodeKurir() {
        return kodeKurir;
    }

    public void setKodeKurir(String kodeKurir) {
        this.kodeKurir = kodeKurir;
    }

    public String getNamaKurir() {
        return namaKurir;
    }

    public void setNamaKurir(String namaKurir) {
        this.namaKurir = namaKurir;
    }

    public String getAlamatKurir() {
        return alamatKurir;
    }

    public void setAlamatKurir(String alamatKurir) {
        this.alamatKurir = alamatKurir;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }
}
