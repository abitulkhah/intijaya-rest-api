package com.kkp.intijaya.IntiJayaKKP.dto;

import java.util.Date;

public class ReceiptDto {
    private String noReceipt;
    private Date tglReceipt;
    private String noSuratJalan;

    public String getNoReceipt() {
        return noReceipt;
    }

    public void setNoReceipt(String noReceipt) {
        this.noReceipt = noReceipt;
    }

    public Date getTglReceipt() {
        return tglReceipt;
    }

    public void setTglReceipt(Date tglReceipt) {
        this.tglReceipt = tglReceipt;
    }

    public String getNoSuratJalan() {
        return noSuratJalan;
    }

    public void setNoSuratJalan(String noSuratJalan) {
        this.noSuratJalan = noSuratJalan;
    }
}
