package com.kkp.intijaya.IntiJayaKKP.dto;

import java.util.Date;

public class DetailReceiptDto {

    private long id;
    private String kodeBarang;
    private String noReceipt;
    private Integer jmlRetur;
    private String ketRetur;
    private Date tglRetur;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public String getNoReceipt() {
        return noReceipt;
    }

    public void setNoReceipt(String noReceipt) {
        this.noReceipt = noReceipt;
    }

    public Integer getJmlRetur() {
        return jmlRetur;
    }

    public void setJmlRetur(Integer jmlRetur) {
        this.jmlRetur = jmlRetur;
    }

    public String getKetRetur() {
        return ketRetur;
    }

    public void setKetRetur(String ketRetur) {
        this.ketRetur = ketRetur;
    }

    public Date getTglRetur() {
        return tglRetur;
    }

    public void setTglRetur(Date tglRetur) {
        this.tglRetur = tglRetur;
    }
}
