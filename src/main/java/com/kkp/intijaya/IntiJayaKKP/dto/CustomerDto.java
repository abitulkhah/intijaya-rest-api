package com.kkp.intijaya.IntiJayaKKP.dto;

public class CustomerDto {

    private String noCustomer;
    private String namaCustomer;
    private String alamat;
    private String noHp;


    public String getNoCustomer() {
        return noCustomer;
    }

    public void setNoCustomer(String noCustomer) {
        this.noCustomer = noCustomer;
    }

    public String getNamaCustomer() {
        return namaCustomer;
    }

    public void setNamaCustomer(String namaCustomer) {
        this.namaCustomer = namaCustomer;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }
}
