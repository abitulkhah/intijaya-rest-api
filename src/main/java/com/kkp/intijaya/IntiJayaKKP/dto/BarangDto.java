package com.kkp.intijaya.IntiJayaKKP.dto;

public class BarangDto {

    private String kodeBarang;
    private String namaBarang;
    private String satuan;
    private Double harga;
    private Integer stok;
    private String kodeModelBarang;

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public Double getHarga() {
        return harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }

    public Integer getStok() {
        return stok;
    }

    public void setStok(Integer stok) {
        this.stok = stok;
    }

    public String getKodeModelBarang() {
        return kodeModelBarang;
    }

    public void setKodeModelBarang(String kodeModelBarang) {
        this.kodeModelBarang = kodeModelBarang;
    }
}
