package com.kkp.intijaya.IntiJayaKKP.dto;

import java.util.Date;

public class SuratJalanDto {
    private String noSuratJalan;
    private Date tglSuratJalan;
    private String namaPenerima;
    private String alamatPenerima;
    private String noInvoice;
    private String kodeKendaraan;
    private String kodeKurir;

    public String getNoSuratJalan() {
        return noSuratJalan;
    }

    public void setNoSuratJalan(String noSuratJalan) {
        this.noSuratJalan = noSuratJalan;
    }

    public Date getTglSuratJalan() {
        return tglSuratJalan;
    }

    public void setTglSuratJalan(Date tglSuratJalan) {
        this.tglSuratJalan = tglSuratJalan;
    }

    public String getNamaPenerima() {
        return namaPenerima;
    }

    public void setNamaPenerima(String namaPenerima) {
        this.namaPenerima = namaPenerima;
    }

    public String getAlamatPenerima() {
        return alamatPenerima;
    }

    public void setAlamatPenerima(String alamatPenerima) {
        this.alamatPenerima = alamatPenerima;
    }

    public String getNoInvoice() {
        return noInvoice;
    }

    public void setNoInvoice(String noInvoice) {
        this.noInvoice = noInvoice;
    }

    public String getKodeKendaraan() {
        return kodeKendaraan;
    }

    public void setKodeKendaraan(String kodeKendaraan) {
        this.kodeKendaraan = kodeKendaraan;
    }

    public String getKodeKurir() {
        return kodeKurir;
    }

    public void setKodeKurir(String kodeKurir) {
        this.kodeKurir = kodeKurir;
    }
}
