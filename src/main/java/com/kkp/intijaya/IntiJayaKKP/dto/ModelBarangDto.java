package com.kkp.intijaya.IntiJayaKKP.dto;

public class ModelBarangDto {

    private String kodeModelBarang;
    private String type;

    public String getKodeModelBarang() {
        return kodeModelBarang;
    }

    public void setKodeModelBarang(String kodeModelBarang) {
        this.kodeModelBarang = kodeModelBarang;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
