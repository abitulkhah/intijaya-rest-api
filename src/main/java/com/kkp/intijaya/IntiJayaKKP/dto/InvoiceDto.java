package com.kkp.intijaya.IntiJayaKKP.dto;

import java.util.Date;

public class InvoiceDto {

    private String noInvoice;
    private String noPemesanan;
    private Date tglInvoice;
    private Double jmlTagihan;


    public String getNoInvoice() {
        return noInvoice;
    }

    public void setNoInvoice(String noInvoice) {
        this.noInvoice = noInvoice;
    }

    public String getNoPemesanan() {
        return noPemesanan;
    }

    public void setNoPemesanan(String noPemesanan) {
        this.noPemesanan = noPemesanan;
    }

    public Date getTglInvoice() {
        return tglInvoice;
    }

    public void setTglInvoice(Date tglInvoice) {
        this.tglInvoice = tglInvoice;
    }

    public Double getJmlTagihan() {
        return jmlTagihan;
    }

    public void setJmlTagihan(Double jmlTagihan) {
        this.jmlTagihan = jmlTagihan;
    }
}
