package com.kkp.intijaya.IntiJayaKKP.utils;

public class GenerateVarID {

    public String getNewID(String latestID, String kodeID){

        int newId = Integer.parseInt(latestID.split("-")[1])+1;
        kodeID = kodeID + newId;
        return kodeID;

    }
}
