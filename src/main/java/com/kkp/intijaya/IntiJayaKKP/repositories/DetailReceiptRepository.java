package com.kkp.intijaya.IntiJayaKKP.repositories;

import com.kkp.intijaya.IntiJayaKKP.model.DetailReceipt;
import com.kkp.intijaya.IntiJayaKKP.model.Pemesanan;
import com.kkp.intijaya.IntiJayaKKP.model.Receipt;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface DetailReceiptRepository extends JpaRepository<DetailReceipt, Long> {
    List<DetailReceipt> findAllByTglReturBetweenOrderByReceiptAsc(Date startTglRetur, Date endTglRetur);
}
