package com.kkp.intijaya.IntiJayaKKP.repositories;

import com.kkp.intijaya.IntiJayaKKP.model.Pemesanan;
import com.kkp.intijaya.IntiJayaKKP.model.SuratJalan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface SuratJalanRepository extends JpaRepository<SuratJalan, Long> {

    SuratJalan findByNoSuratJalan(String noSuratJalan);

    @Modifying
    @Transactional
    void deleteByNoSuratJalan(String noSuratJalan);

    List<SuratJalan> findAllByOrderByNoSuratJalanAsc();

    List<SuratJalan> findAllByTglSuratJalanBetweenOrderByTglSuratJalanAsc(Date startTglPemesanan, Date endTglPemesanan);

}
