package com.kkp.intijaya.IntiJayaKKP.repositories;

import com.kkp.intijaya.IntiJayaKKP.model.ModelBarang;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ModelBarangRepository extends JpaRepository<ModelBarang, Long> {

    ModelBarang findByKodeModelBarang(String kodeModelBarang);

    @Modifying
    @Transactional
    void deleteByKodeModelBarang(String kodeModelBarang);

    List<ModelBarang> findAllByOrderByKodeModelBarangAsc();

}
