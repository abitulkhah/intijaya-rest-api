package com.kkp.intijaya.IntiJayaKKP.repositories;

import com.kkp.intijaya.IntiJayaKKP.model.Barang;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BarangRepository extends JpaRepository<Barang, Long> {

    Barang findByKodeBarang(String kodeBarang);

    @Modifying
    @Transactional
    void deleteByKodeBarang(String kodeBarang);

    List<Barang> findAllByOrderByKodeBarangAsc();
}
