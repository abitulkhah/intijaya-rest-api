package com.kkp.intijaya.IntiJayaKKP.repositories;

import com.kkp.intijaya.IntiJayaKKP.model.Kwitansi;
import com.kkp.intijaya.IntiJayaKKP.model.Pemesanan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface KwitansiRepository extends JpaRepository<Kwitansi, Long> {

    Kwitansi findByNoKwitansi(String noKwitansi);

    @Modifying
    @Transactional
    void deleteByNoKwitansi(String noKwitansi);

    List<Kwitansi> findAllByOrderByNoKwitansiAsc();

    List<Kwitansi> findAllByTglKwitansiBetweenOrderByTglKwitansiAsc(Date startTglPemesanan, Date endTglPemesanan);


}
