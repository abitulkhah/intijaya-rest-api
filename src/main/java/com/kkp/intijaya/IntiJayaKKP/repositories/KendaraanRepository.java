package com.kkp.intijaya.IntiJayaKKP.repositories;

import com.kkp.intijaya.IntiJayaKKP.model.Kendaraan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface KendaraanRepository extends JpaRepository<Kendaraan, Long> {

    Kendaraan findByKodeKendaraan(String kodeKendaraan);

    @Modifying
    @Transactional
    void deleteByKodeKendaraan(String kodeKendaraan);

    List<Kendaraan> findAllByOrderByKodeKendaraanAsc();

}
