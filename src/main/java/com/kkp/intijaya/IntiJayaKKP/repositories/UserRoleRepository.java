package com.kkp.intijaya.IntiJayaKKP.repositories;

import com.kkp.intijaya.IntiJayaKKP.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
}
