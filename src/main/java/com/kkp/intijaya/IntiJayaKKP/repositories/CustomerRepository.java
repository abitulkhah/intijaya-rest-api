package com.kkp.intijaya.IntiJayaKKP.repositories;

import com.kkp.intijaya.IntiJayaKKP.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Customer findByNoCustomer(String noCustomer);

    @Modifying
    @Transactional
    void deleteByNoCustomer(String noCustomer);

    List<Customer> findAllByOrderByNoCustomerAsc();
}
