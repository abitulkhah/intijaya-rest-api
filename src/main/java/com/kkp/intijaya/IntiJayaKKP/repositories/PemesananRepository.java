package com.kkp.intijaya.IntiJayaKKP.repositories;

import com.kkp.intijaya.IntiJayaKKP.model.Pemesanan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface PemesananRepository extends JpaRepository<Pemesanan, Long> {

    Pemesanan findByNoPemesanan(String noPemesanan);

    @Modifying
    @Transactional
    void deleteByNoPemesanan(String noPemesanan);

    List<Pemesanan> findAllByOrderByNoPemesananAsc();

    List<Pemesanan> findAllByTglPemesananBetween(Date startTglPemesanan, Date endTglPemesanan);

}
