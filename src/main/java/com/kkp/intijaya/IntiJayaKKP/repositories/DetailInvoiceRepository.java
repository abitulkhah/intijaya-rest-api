package com.kkp.intijaya.IntiJayaKKP.repositories;

import com.kkp.intijaya.IntiJayaKKP.model.DetailInvoice;
import com.kkp.intijaya.IntiJayaKKP.model.DetailPemesanan;
import com.kkp.intijaya.IntiJayaKKP.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DetailInvoiceRepository extends JpaRepository<DetailInvoice, Long> {
    List<DetailInvoice> findAllByInvoice(Invoice invoice);
}
