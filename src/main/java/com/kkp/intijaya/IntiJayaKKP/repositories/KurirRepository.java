package com.kkp.intijaya.IntiJayaKKP.repositories;

import com.kkp.intijaya.IntiJayaKKP.model.Kurir;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface KurirRepository extends JpaRepository<Kurir, Long> {

    Kurir findByKodeKurir(String kodeKurir);

    @Modifying
    @Transactional
    void deleteByKodeKurir(String kodeKurir);

    List<Kurir> findAllByOrderByKodeKurirAsc();

}
