package com.kkp.intijaya.IntiJayaKKP.repositories;

import com.kkp.intijaya.IntiJayaKKP.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    Invoice findByNoInvoice(String noInvoice);

    @Modifying
    @Transactional
    void deleteByNoInvoice(String noInvoice);

    List<Invoice> findAllByOrderByNoInvoiceAsc();

}
