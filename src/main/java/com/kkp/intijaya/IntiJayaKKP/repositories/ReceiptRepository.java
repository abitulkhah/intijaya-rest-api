package com.kkp.intijaya.IntiJayaKKP.repositories;

import com.kkp.intijaya.IntiJayaKKP.model.DetailReceipt;
import com.kkp.intijaya.IntiJayaKKP.model.Receipt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface ReceiptRepository extends JpaRepository<Receipt, Long> {

    Receipt findByNoReceipt(String noReceipt);

    @Modifying
    @Transactional
    void deleteByNoReceipt(String noReceipt);

    List<Receipt> findAllByOrderByNoReceiptAsc();

}
