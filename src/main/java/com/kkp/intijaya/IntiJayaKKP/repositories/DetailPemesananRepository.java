package com.kkp.intijaya.IntiJayaKKP.repositories;

import com.kkp.intijaya.IntiJayaKKP.model.DetailPemesanan;
import com.kkp.intijaya.IntiJayaKKP.model.Pemesanan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DetailPemesananRepository extends JpaRepository<DetailPemesanan, Long> {
    List<DetailPemesanan> findAllByPemesanan(Pemesanan pemesanan);
}
