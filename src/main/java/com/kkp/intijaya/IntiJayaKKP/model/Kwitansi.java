package com.kkp.intijaya.IntiJayaKKP.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "kwitansi")
public class Kwitansi {
    @Id
    @Column(name = "no_kwitansi", length = 10, nullable = false, updatable = false)
    private String noKwitansi;

    @Column(name = "tgl_kwitansi")
    @Temporal (TemporalType.TIMESTAMP)
    @CreatedDate
    private Date tglKwitansi;

    @Column(name = "jml_bayar")
    private Double jmlBayar;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "no_invoice", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Invoice invoice;


    public String getNoKwitansi() {
        return noKwitansi;
    }

    public void setNoKwitansi(String noKwitansi) {
        this.noKwitansi = noKwitansi;
    }

    public Date getTglKwitansi() {
        return tglKwitansi;
    }

    public void setTglKwitansi(Date tglKwitansi) {
        this.tglKwitansi = tglKwitansi;
    }

    public Double getJmlBayar() {
        return jmlBayar;
    }

    public void setJmlBayar(Double jmlBayar) {
        this.jmlBayar = jmlBayar;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}
