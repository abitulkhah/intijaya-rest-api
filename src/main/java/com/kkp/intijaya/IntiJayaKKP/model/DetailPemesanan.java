package com.kkp.intijaya.IntiJayaKKP.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "detail_pemesanan")
public class DetailPemesanan {

    @Id
    @GeneratedValue(generator = "detail_pemesanan_generator")
    @SequenceGenerator(
            name = "detail_pemesanan_generator",
            sequenceName = "detail_pemesanan_sequence",
            initialValue = 2
    )
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "no_pemesanan", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Pemesanan pemesanan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kode_barang", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Barang barang;

    @Column(name = "jml_pemesanan")
    private Integer jmlPemesanan;

    @Column(name = "ket_barang")
    private String ketBarang;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pemesanan getPemesanan() {
        return pemesanan;
    }

    public void setPemesanan(Pemesanan pemesanan) {
        this.pemesanan = pemesanan;
    }

    public Barang getBarang() {
        return barang;
    }

    public void setBarang(Barang barang) {
        this.barang = barang;
    }

    public Integer getJmlPemesanan() {
        return jmlPemesanan;
    }

    public void setJmlPemesanan(Integer jmlPemesanan) {
        this.jmlPemesanan = jmlPemesanan;
    }

    public String getKetBarang() {
        return ketBarang;
    }

    public void setKetBarang(String ketBarang) {
        this.ketBarang = ketBarang;
    }
}
