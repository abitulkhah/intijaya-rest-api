package com.kkp.intijaya.IntiJayaKKP.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "receipt")
public class Receipt {

    @Id
    @Column(name = "no_receipt", length = 10, nullable = false, updatable = false)
    private String noReceipt;

    @Column(name = "tgl_receipt")
    @Temporal (TemporalType.TIMESTAMP)
    @CreatedDate
    private Date tglReceipt;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "no_surat_jalan", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private SuratJalan suratJalan;

    public String getNoReceipt() {
        return noReceipt;
    }

    public void setNoReceipt(String noReceipt) {
        this.noReceipt = noReceipt;
    }

    public Date getTglReceipt() {
        return tglReceipt;
    }

    public void setTglReceipt(Date tglReceipt) {
        this.tglReceipt = tglReceipt;
    }

    public SuratJalan getSuratJalan() {
        return suratJalan;
    }

    public void setSuratJalan(SuratJalan suratJalan) {
        this.suratJalan = suratJalan;
    }
}
