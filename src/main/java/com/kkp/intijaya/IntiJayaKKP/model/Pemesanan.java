package com.kkp.intijaya.IntiJayaKKP.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "pemesanan")
public class Pemesanan {

    @Id
    @Column(name = "no_pemesanan", length = 10, nullable = false, updatable = false)
    private String noPemesanan;

    @Column(name = "tgl_pemesanan")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date tglPemesanan;

    @Column(name = "dp")
    private Double dp;

    @Column(name = "sisa_bayar")
    private Double sisaBayar;

    @Column(name = "grand_total")
    private Double grandTotal;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "no_customer", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Customer customer;

    public String getNoPemesanan() {
        return noPemesanan;
    }

    public void setNoPemesanan(String noPemesanan) {
        this.noPemesanan = noPemesanan;
    }

    public Date getTglPemesanan() {
        return tglPemesanan;
    }

    public void setTglPemesanan(Date tglPemesanan) {
        this.tglPemesanan = tglPemesanan;
    }

    public Double getDp() {
        return dp;
    }

    public void setDp(Double dp) {
        this.dp = dp;
    }

    public Double getSisaBayar() {
        return sisaBayar;
    }

    public void setSisaBayar(Double sisaBayar) {
        this.sisaBayar = sisaBayar;
    }

    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
