package com.kkp.intijaya.IntiJayaKKP.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "detail_receipt")
public class DetailReceipt {


    @Id
    @GeneratedValue(generator = "detail_receipt_generator")
    @SequenceGenerator(
            name = "detail_receipt_generator",
            sequenceName = "detail_receipt_sequence",
            initialValue = 10
    )
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kode_barang", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Barang barang;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "no_receipt", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Receipt receipt;

    @Column(name = "jml_retur")
    private Integer jmlRetur;

    @Column(name = "ket_retur")
    private String ketRetur;


    @Column(name = "tgl_retur")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date tglRetur;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Barang getBarang() {
        return barang;
    }

    public void setBarang(Barang barang) {
        this.barang = barang;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public Integer getJmlRetur() {
        return jmlRetur;
    }

    public void setJmlRetur(Integer jmlRetur) {
        this.jmlRetur = jmlRetur;
    }

    public String getKetRetur() {
        return ketRetur;
    }

    public void setKetRetur(String ketRetur) {
        this.ketRetur = ketRetur;
    }

    public Date getTglRetur() {
        return tglRetur;
    }

    public void setTglRetur(Date tglRetur) {
        this.tglRetur = tglRetur;
    }
}
