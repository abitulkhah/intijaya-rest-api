package com.kkp.intijaya.IntiJayaKKP.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @Column(name = "no_customer", length = 10, nullable = false, updatable = false)
    private String noCustomer;

    @Column(name = "nama_customer")
    private String namaCustomer;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "no_hp", length = 20)
    private String noHp;

    public String getNoCustomer() {
        return noCustomer;
    }

    public void setNoCustomer(String noCustomer) {
        this.noCustomer = noCustomer;
    }

    public String getNamaCustomer() {
        return namaCustomer;
    }

    public void setNamaCustomer(String namaCustomer) {
        this.namaCustomer = namaCustomer;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }
}
