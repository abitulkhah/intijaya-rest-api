package com.kkp.intijaya.IntiJayaKKP.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;


@Entity
@Table(name = "kendaraan")
public class Kendaraan {
    @Id
    @Column(name = "kode_kendaraan", length = 10, nullable = false, updatable = false)
    private String kodeKendaraan;

    @Column(name = "type_kendaraan")
    private String typeKendaraan;

    @Column(name = "nama_kendaraan")
    private String namaKendaraan;

    @Column(name = "no_pol")
    private String nopol;

    public String getKodeKendaraan() {
        return kodeKendaraan;
    }

    public void setKodeKendaraan(String kodeKendaraan) {
        this.kodeKendaraan = kodeKendaraan;
    }

    public String getTypeKendaraan() {
        return typeKendaraan;
    }

    public void setTypeKendaraan(String typeKendaraan) {
        this.typeKendaraan = typeKendaraan;
    }

    public String getNamaKendaraan() {
        return namaKendaraan;
    }

    public void setNamaKendaraan(String namaKendaraan) {
        this.namaKendaraan = namaKendaraan;
    }

    public String getNopol() {
        return nopol;
    }

    public void setNopol(String nopol) {
        this.nopol = nopol;
    }
}
