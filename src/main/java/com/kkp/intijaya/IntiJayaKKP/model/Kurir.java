package com.kkp.intijaya.IntiJayaKKP.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;


@Entity
@Table(name = "kurir")
public class Kurir {
    @Id
    @Column(name = "kode_kurir", length = 10, nullable = false, updatable = false)
    private String kodeKurir;

    @Column(name = "nama_kurir")
    private String namaKurir;

    @Column(name = "alamat_kurir")
    private String alamatKurir;

    @Column(name = "no_hp")
    private String noHp;

    public String getKodeKurir() {
        return kodeKurir;
    }

    public void setKodeKurir(String kodeKurir) {
        this.kodeKurir = kodeKurir;
    }

    public String getNamaKurir() {
        return namaKurir;
    }

    public void setNamaKurir(String namaKurir) {
        this.namaKurir = namaKurir;
    }

    public String getAlamatKurir() {
        return alamatKurir;
    }

    public void setAlamatKurir(String alamatKurir) {
        this.alamatKurir = alamatKurir;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }
}
