package com.kkp.intijaya.IntiJayaKKP.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;
import sun.security.util.Pem;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "invoice")
public class Invoice {

    @Id
    @Column(name = "no_invoice", length = 10, nullable = false, updatable = false)
    private String noInvoice;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "no_pemesanan", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Pemesanan pemesanan;

    @Column(name = "tgl_invoice")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date tglInvoice;

    @Column(name = "jml_tagihan")
    private Double jmlTagihan;

    public String getNoInvoice() {
        return noInvoice;
    }

    public void setNoInvoice(String noInvoice) {
        this.noInvoice = noInvoice;
    }

    public Pemesanan getPemesanan() {
        return pemesanan;
    }

    public void setPemesanan(Pemesanan pemesanan) {
        this.pemesanan = pemesanan;
    }

    public Date getTglInvoice() {
        return tglInvoice;
    }

    public void setTglInvoice(Date tglInvoice) {
        this.tglInvoice = tglInvoice;
    }

    public Double getJmlTagihan() {
        return jmlTagihan;
    }

    public void setJmlTagihan(Double jmlTagihan) {
        this.jmlTagihan = jmlTagihan;
    }
}
