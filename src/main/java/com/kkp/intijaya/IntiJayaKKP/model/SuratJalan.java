package com.kkp.intijaya.IntiJayaKKP.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "surat_jalan")
public class SuratJalan {

    @Id
    @Column(name = "no_surat_jalan", length = 10, nullable = false, updatable = false)
    private String noSuratJalan;

    @Column(name = "tgl_surat_jalan")
    @Temporal (TemporalType.TIMESTAMP)
    @CreatedDate
    private Date tglSuratJalan;

    @Column(name = "nama_penerima")
    private String namaPenerima;

    @Column(name = "alamat_penerima")
    private String alamatPenerima;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kode_kurir", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Kurir kurir;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "no_invoice", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Invoice invoice;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kode_kendaraan", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Kendaraan kendaraan;

    public String getNoSuratJalan() {
        return noSuratJalan;
    }

    public void setNoSuratJalan(String noSuratJalan) {
        this.noSuratJalan = noSuratJalan;
    }

    public Date getTglSuratJalan() {
        return tglSuratJalan;
    }

    public void setTglSuratJalan(Date tglSuratJalan) {
        this.tglSuratJalan = tglSuratJalan;
    }

    public String getNamaPenerima() {
        return namaPenerima;
    }

    public void setNamaPenerima(String namaPenerima) {
        this.namaPenerima = namaPenerima;
    }

    public String getAlamatPenerima() {
        return alamatPenerima;
    }

    public void setAlamatPenerima(String alamatPenerima) {
        this.alamatPenerima = alamatPenerima;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public Kurir getKurir() {
        return kurir;
    }

    public void setKurir(Kurir kurir) {
        this.kurir = kurir;
    }

    public Kendaraan getKendaraan() {
        return kendaraan;
    }

    public void setKendaraan(Kendaraan kendaraan) {
        this.kendaraan = kendaraan;
    }
}
