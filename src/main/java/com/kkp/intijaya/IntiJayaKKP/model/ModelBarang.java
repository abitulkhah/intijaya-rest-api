package com.kkp.intijaya.IntiJayaKKP.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "model_barang")
public class ModelBarang {
    @Id
    @Column(name = "kode_model_barang", length = 10, nullable = false, updatable = false)
    private String kodeModelBarang;

    @Column(name = "type")
    private String type;

    public String getKodeModelBarang() {
        return kodeModelBarang;
    }

    public void setKodeModelBarang(String kodeModelBarang) {
        this.kodeModelBarang = kodeModelBarang;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
