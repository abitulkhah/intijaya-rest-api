INSERT INTO role (id, role_name, description) VALUES (1, 'STANDARD_USER', 'Standard User - Has no admin');
INSERT INTO role (id, role_name, description) VALUES (2, 'ADMIN_USER', 'Admin User - Has permission to perform admin tasks');


INSERT INTO public.users (id, full_name, password, username, email) VALUES (1, 'Meilina Putri', '$2y$12$IK7itFEnuhx9Gf6w0gS/z.OTy3H84dmHqfHaVA485qzy1I1QQujdy', 'meilina.putri','meilina@gmail.com');
INSERT INTO public.users (id, full_name, password, username,  email) VALUES (2, 'Abi Tulkhah', '$2y$12$IK7itFEnuhx9Gf6w0gS/z.OTy3H84dmHqfHaVA485qzy1I1QQujdy', 'abi.tulkhah','abi.tulkhah@gmail.com');

INSERT INTO user_role(id, user_id, role_id) VALUES(1, 1, 1);
INSERT INTO user_role(id, user_id, role_id) VALUES(2, 2, 1);
INSERT INTO user_role(id, user_id, role_id) VALUES(3, 2, 2);

